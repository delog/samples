# coding: utf-8

import pandas as pd
import numpy as np
import scipy

from sklearn.linear_model import Ridge, LogisticRegression, Lasso, ElasticNet, SGDRegressor
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.preprocessing import LabelBinarizer
from sklearn.decomposition import TruncatedSVD
from sklearn.metrics import mean_squared_log_error 

import lightgbm as lgb
from lightgbm import LGBMRegressor

from nltk.stem import WordNetLemmatizer
from nltk.stem.porter import PorterStemmer
from nltk.stem.lancaster import LancasterStemmer

import wordbatch

from wordbatch.extractors import WordBag, WordHash
from wordbatch.models import FTRL, FM_FTRL

import csv
import math
import time
import gc


# In[2]:

NUM_BRANDS = 1100  
NAME_MIN_DF = 7
MAX_FEAT_DESCP = 50000

NAME_STOP_WORDS = [
    'lululemon', 'vault', 'controller', 'blaze', 'Vuitton', 'vuitton',
    'dustbag', 'dust', 'bear', 'inch', 'macbook', 'dre', 'vuitton box', 'alta',
    'rm', 'amiibo', 'shopping', 'penny', 'lululemon', 'charge']


DESC_STOP_WORDS = [
    'envelope', 'fitbit', 'tracking', 'stamp', 'rm', '[rm]', 'ultra', '14k',
    'Vial', 'fine', 'hours', 'x13', 'sample']


# data reading
def read_data(train_mode = True, remove_nulls = True) :
    print("Reading in Data")
    df_train = pd.read_csv('../input/train.tsv', sep='\t', encoding='utf-8', engine='python')
    if not train_mode :
        df_test = pd.read_csv('../input/test.tsv', sep='\t', encoding='utf-8', engine='python')
    else :   
        df_train, df_test = train_test_split(df_train, test_size = 0.05, shuffle = False)
    
    print('train size = ', df_train.shape[0])
    print('test size = ', df_test.shape[0])
    
    # remove null prices
    if remove_nulls :
        df_train = df_train.drop(df_train[(df_train.price < 1.0)].index)
    
    return df_train, df_test
    
# data preparation, vectorization, converting to sparse matrix
def encode(df) :
    df["category_name"] = df["category_name"].fillna("Other").astype("category")

    df["brand_name"] = df["brand_name"].fillna("unknown")
    pop_brands = df["brand_name"].value_counts().index[:NUM_BRANDS]
    df.loc[~df["brand_name"].isin(pop_brands), "brand_name"] = "Other"
    df["brand_name"] = df["brand_name"].astype("category")

    df["item_description"] = df["item_description"].fillna("None")
    df.loc[df["item_description"] == 'No description yet', "item_description"] = "[ndy]"

    df["item_condition_id"] = df["item_condition_id"].astype("category")

    count_name = CountVectorizer(min_df=NAME_MIN_DF, ngram_range = (1,2), stop_words = NAME_STOP_WORDS)
    X_name = count_name.fit_transform(df["name"])

    print("Brand encoders")
    vect_brand = LabelBinarizer(sparse_output=True)
    X_brand = vect_brand.fit_transform(df["brand_name"])

    print("Category Encoders")
    unique_categories = pd.Series("/".join(df["category_name"].unique().astype("str")).split("/")).unique()
    count_category = CountVectorizer()
    X_category = count_category.fit_transform(df["category_name"])

    print('Splitted categories')    
    df['general_cat'], df['subcat_1'], df['subcat_2'] = df['category_name'].str.split("/", 2).str
    df['general_cat'] = df['general_cat'].fillna('Other').astype(str)
    df['subcat_1'] = df['subcat_1'].fillna('Other').astype(str)
    df['subcat_2'] = df['subcat_2'].fillna('Other').astype(str)
    
    count_gen_cat = CountVectorizer()
    X_gen_cat = count_gen_cat.fit_transform(df['general_cat'])
    print('category vocabulary 0 length = ', len(count_gen_cat.vocabulary_))
    
    count_cat_1 = CountVectorizer()
    X_cat_1 = count_cat_1.fit_transform(df['subcat_1'])
    print('category vocabulary 1 length = ', len(count_cat_1.vocabulary_))
    
    count_cat_2 = CountVectorizer()
    X_cat_2 = count_cat_2.fit_transform(df['subcat_2'])
    print('category vocabulary 2 length = ', len(count_cat_2.vocabulary_))
    
    print("Description encoders")
    count_descp = TfidfVectorizer(max_features = MAX_FEAT_DESCP, 
                                  ngram_range = (1,4),
                                  stop_words = DESC_STOP_WORDS)
    X_descp = count_descp.fit_transform(df["item_description"])

    print("Dummy Encoders")
    X_dummies = scipy.sparse.csr_matrix(pd.get_dummies(df[[
        "item_condition_id", "shipping"]], sparse = True).values)

    X = scipy.sparse.hstack((X_dummies,
                             X_descp,
                             X_brand,
                             X_category,
                             X_cat_2,
                             X_name)).tocsr()

    print('sparse shape before cutting', X.shape)
    mask = np.array(np.clip(X.getnnz(axis=0) - 1, 0, 1), dtype=bool)
    X = X[:, mask]
    print('sparse shape after cutting', X.shape)
    
    return X

# ridge model
def predict_ridge(X_train, y_train, X_test) :
    model = Ridge(solver = "auto", fit_intercept = True, alpha = 1, max_iter = 15) 

    print('Ridge ', model)
    model.fit(X_train, y_train)

    ridge_preds = model.predict(X_test)

    if (train_mode):
        error = rmsle2(np.asarray(y_test), np.expm1(ridge_preds))
        print('error ridge', error)

    return ridge_preds

# FTRL model    
def predict_ftrl(X_train, y_train, X_test) :
    model = FM_FTRL(alpha=0.01, beta=0.01, L1=0.00001, L2=0.1, D=X.shape[1], alpha_fm=0.01, L2_fm=0.0, init_fm=0.01,
                    D_fm=200, e_noise=0.0001, iters=15, inv_link="identity", threads=4)
    print('FM_FTRL ', model)
    model.fit(X_train.astype(np.float), y_train.astype(np.float))
    ftrl_preds = model.predict(X_test.astype(np.float))

    if (train_mode):
        error = rmsle2(np.asarray(y_test), np.expm1(ftrl_preds))
        print('error ftrl', error)

    return ftrl_preds

def rmsle_lgb(labels, preds):
    return 'rmsle', rmsle(preds, labels), False

# LightGBM model
def predict_lgbm1(X_train, y_train, X_test) :
    X_train2, X_test2, y_train2, y_test2 = train_test_split(X_train, y_train, test_size=0.3, random_state=42)
    lgbm_params = {'n_estimators': 1000, 'learning_rate': 0.4, 'max_depth': 10,
                   'num_leaves': 31, 'subsample': 0.9, 'colsample_bytree': 0.8,
                   'min_child_samples': 50, 'n_jobs': 4}
    model = LGBMRegressor(**lgbm_params)
    model.fit(X_train2, y_train2,
             eval_set=[(X_test2, y_test2)],
             eval_metric=rmsle_lgb,
             early_stopping_rounds=100,
             verbose=True)

    lgbm_preds = model.predict(X_test)
    return lgbm_preds

# RMSLE
def rmsle2(y, y_pred):
    assert len(y) == len(y_pred)
    terms_to_sum = [(math.log(y_pred[i] + 1) - math.log(y[i] + 1)) ** 2.0 for i,pred in enumerate(y_pred)]
    return (sum(terms_to_sum) * (1.0/len(y))) ** 0.5


# main code
train_mode = False

df_train, df_test = read_data(train_mode = train_mode)

nrow_train = df_train.shape[0]

# joining train and test set
df = pd.concat([df_train, df_test], 0)
nrow_train = df_train.shape[0]
print('train size = ', df_train.shape[0])
print('all size = ', df.shape[0])

y_train = np.log1p(df_train["price"])
if (train_mode):
    y_test = df["price"][nrow_train:]

del df_train
del df_test
gc.collect()

# encoding
X = encode(df)

X_train = X[:nrow_train]
X_test = X[nrow_train:]   

ftrl_preds = predict_ftrl(X_train, y_train, X_test)
if (train_mode):
    error = rmsle2(np.asarray(y_test), np.expm1(ftrl_preds))
    print('error ftrl', error)

ridge_preds = predict_ridge(X_train, y_train, X_test)
if (train_mode):
    error = rmsle2(np.asarray(y_test), np.expm1(ridge_preds))
    print('error ridge', error)

lgbm1_preds = predict_lgbm1(X_train, y_train, X_test)
if (train_mode):
    error = rmsle2(np.asarray(y_test), np.expm1(lgbm1_preds))
    print('error lgbm1', error)

# ensemble
preds = 0.3 * ridge_preds + 0.4 * ftrl_preds + 0.3 * lgbm1_preds

if (train_mode):
    error = rmsle2(np.asarray(y_test), np.expm1(preds))
    print('error ensemble', error)
else :    
    df_test['price'] =  np.expm1(preds)

    df_test[['test_id', 'price']].to_csv("submission.csv", index=False)
    
